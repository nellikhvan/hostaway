<?php

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'first_name'   => $faker->firstName,
        'last_name'    => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'country_code' => $faker->countryCode,
        'timezone'     => $faker->timezone,
        'created_at'   => now(),
        'updated_at'   => now()
    ];
});
