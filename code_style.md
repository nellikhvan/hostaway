# PHP: Code Style

Since most of the platform's projects are based on [Yii](https://github.com/yiisoft) frameworks, code style is inherited from the [Yii 2 Core Framework Code Style](https://github.com/yiisoft/yii2/blob/master/docs/internals/core-code-style.md) which is [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) compatible.

This document describes platform-specific additions to [Yii 2 Core Framework Code Style](https://github.com/yiisoft/yii2/blob/master/docs/internals/core-code-style.md).

## IDE Settings

It is RECOMMENDED to use `Default` `PHPStorm` editor's code style settings for code formatting.

## Code Style

### Overview

- `private` properties and methods SHOULD NOT be prefixed with underscore `_`.
- There SHOULD NOT be more than one space in a row in the middle of the line.
- It's NOT RECOMMENDED to put a whitespace if it's not needed.

### Line Width

- According to `PSR` line width is hard-limited to 120 symbols and soft-limited to 80 symbols. It's NOT RECOMMENDED to exceed soft limit if it's not necessary.

### Namespaces

- All namespaces MUST be [PSR-4](https://www.php-fig.org/psr/psr-4/) compliant.
- If class is not imported root namespace MUST be specified (e.g. `\Yii::app()`)

### Control Statements

- Control statements SHOULD NOT be nested more than 3-levels deep.

```php
<?php

// good
function foo1()
{
    if (!true) {
        return false;
    }

    if (!true) {
        return false;
    }

    if (!true) {
        return false;
    }

    return true;
}

// bad
function foo2()
{
    if (true) {
        if (true) {
            if (true) {
                return true;
            }
        }
    }
    
    return false;
}
```

### Strings

- It's RECOMMENDED to use `\sprintf()` or `\implode()` instead of `.`  for strings concatenation.
- It's RECOMMENDED to use `,` with `echo` instead of `.` concatenation (e.g. `echo $string1, $string2;`).

### DB/PDO/ActiveRecord

- Params binding MUST always be used instead of condition string concatenation.

### Documentation

- In case of deprecation of any code structure it MUST be annotated with `@deprecated` `phpDoc` tag.
- All class properties, variables, method arguments should be annotated with `@property`, `@var`, `@param` `phpDoc` tags with types correctly specified.
- Method or function signature SHOULD contain implicit type declarations compatible with `PHP 5.6` (e.g. `public function foo(ClassName $object, array $params = [], callable $callback = null);`).
