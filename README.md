This project is a test project for hostaway. Not Finished yet.

### Installation
> docker-compose up -d


run
> docker-compose ps

go inside php container
> docker exec -t -i hostaway_web_1 /bin/bash

run
> composer update

If there is no  *barryvdh/laravel-ide-helper* in vendor.
run
> composer require barryvdh/laravel-ide-helper

run migrations
> php artisan migrate

run helper files
> php artisan ide-helper:meta

> php artisan ide-helper:generate

Add *.phpstorm.meta.php* and *_ide_helper.php* to .gitignore.



